package main

import (
	"baitap1/api"
	"net/http"
	"github.com/gorilla/mux"
)

func main(){
	r := mux.NewRouter()
	http.HandleFunc("/admin/admin",api.LoginApi)
	r.HandleFunc("/api-chuoi",api.AuthenMiddleJWT(api.AddApi)).Methods("POST")
	r.HandleFunc("/api-chuoi",api.AuthenMiddleJWT(api.GetApi)).Methods("GET")
	http.ListenAndServe(":8000",r)

}
