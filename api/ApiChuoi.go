package api

import (
	"baitap1/model"
	"baitap1/repository"
	"encoding/json"
	"log"
	"net/http"

)

 func AddApi(w http.ResponseWriter, r *http.Request){
		decode := json.NewDecoder(r.Body)
		var chuoi model.Chuoi
		err := decode.Decode(&chuoi)
		if err != nil{
			log.Panic(err)
		}else {
			repository.AddChuoi(chuoi)
		}

}
func GetApi(w http.ResponseWriter, r *http.Request){
		ds := repository.GetChuoi()
		var i int
		for i = 0; i < len(ds); i++ {
			body, err := json.Marshal(ds[i])
			if err != nil {
				log.Println(err)
			} else {
				w.Write(body)
			}
		}
}