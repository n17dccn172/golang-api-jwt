package api

import (
	"baitap1/message"
	"baitap1/model"
	"baitap1/utils"
	"encoding/json"
	"net/http"
)

const Name = "thien"
const Pass = "123456"

var LoginApi = func(w http.ResponseWriter, r *http.Request) {
	var loginData model.Admin
	err := json.NewDecoder(r.Body).Decode(&loginData)
	if err != nil {
		message.ResponseErr(w, http.StatusBadRequest)
		return
	}
	admin := model.Admin{
		Name,
		Pass,
	}
	if admin ==  loginData{
		var tokenString string
		tokenString, err = utils.GenToken(admin.Name)
		if err != nil {
			message.ResponseErr(w, http.StatusInternalServerError)
			return
		}

		message.ResponseOk(w, model.RegisterResponse{
			Token:  tokenString,
			Status: http.StatusOK,
		})
	}else {
		message.ResponseErr(w, http.StatusUnauthorized)
		return
	}
}

func AuthenMiddleJWT(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tokenHeader := r.Header.Get("Authorization")

		if tokenHeader == "" {
			message.ResponseErr(w, http.StatusUnauthorized)

		}
		if utils.ValiToken(tokenHeader) {
			next.ServeHTTP(w,r)
		}

	}

}



