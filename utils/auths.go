package utils

import (

	"baitap1/model"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"strings"
)
var JwtKey = []byte("ThienLaAdmin")

func GenToken(name string) (string, error) {
	//expirationTime := time.Now().Add(120 * time.Second)
	claims := &model.Claims{
		Name: name,
		//StandardClaims: jwt.StandardClaims{
		//	ExpiresAt: expirationTime.Unix(),
		//},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(JwtKey)
}
func ValiToken(tokenHeader string) bool{
	splitted := strings.Split(tokenHeader, " ") // Bearer jwt_token
	if len(splitted) != 2 {
		return false
	}

	tokenPart := splitted[1]
	tk := &model.Claims{}

	token, err := jwt.ParseWithClaims(tokenPart, tk, func(token *jwt.Token) (interface{}, error) {
		return JwtKey, nil
	})

	if err != nil {
		fmt.Println(err)
		return false
	}

	if  !token.Valid {
		//message.ResponseOk(w, token.Claims)
		//next.ServeHTTP(w,r)
		return false
	}
	return true
}