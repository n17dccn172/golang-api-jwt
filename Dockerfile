FROM golang

WORKDIR /go/src/app

COPY . .

RUN go get -d -v ./...

RUN go install -v ./...

RUN go build -o ./baitap1

CMD ["./baitap1"]
