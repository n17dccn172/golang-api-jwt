package repository
import (
	"baitap1/driver"
	"baitap1/model"
	"log"
)

func AddChuoi(chuoi model.Chuoi) {
	db := driver.Connect()
	state:= "INSERT INTO  chuoi (value) VALUES ($1)"
	_, err := db.Exec(state,chuoi.Value)
	if err != nil {
		log.Panic(err)
	}
	db.Close()
}
func GetChuoi() []model.Chuoi{
	db := driver.Connect()
	ds := []model.Chuoi{}
	state:= "SELECT * FROM CHUOI"
	rs, err := db.Query(state)
	if err != nil {
		log.Panic(err)
	}else{
		for rs.Next(){
			c := model.Chuoi{}
			err1 := rs.Scan(&c.Id,&c.Value)
			if err1 != nil{
				log.Fatal(err1)
			}else{
				ds = append(ds,c)
			}
		}
		db.Close()

	}
	return ds
}
